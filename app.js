var server = require('http').createServer();

var fs = require('fs');
var Url = require('url');
var Path = require('path');
var qs = require('querystring');
server.on('request',function(req,res){
	var url = Url.parse(req.url)
	// console.log(url)
	if(req.url==='/'){
		res.writeHead('200',{
			"Content-Type":"text/html;charset=utf-8"
		})
		fs.readFile('./weaterfall.html',function(err,data){
			if(err){
				throw err
			}
			res.end(data)
		})
	}else if(req.url=='/getFile'){
		var data;
		var response = {};
		if(req.method.toLowerCase()==='post'){
			var str = '';
			req.on('data',function(chunk){
				str += chunk.toString();
			})
			req.on('end',function(){
				data = qs.parse(str)
				var offset = Number(data && data.offset||0);
				var limit = Number(data && data.limit||10);
				var allfiles = getFile('./static/img');
				var length = allfiles.length;
				if(offset > length-1) {
					response.message = '对不起,索引位置超出';
					response.data = [];
				}else {
					if(offset+limit >= length-1){
						response.message = '没有更多了'
						response.data = allfiles.slice(offset)
					}else {
						response.message = '获取成功'
						response.data = allfiles.slice(offset,offset+limit);
					}
				}
				res.writeHead('200',{
					"Content-Type":"application/json"
				})
				res.end(JSON.stringify(response))
			})
			
		}
	}else if(/^\/static\/img/.test(req.url)) {
		fs.readFile('.'+req.url,'binary',function(err,data){
			if(err){
				throw err
			}
			res.writeHead(200,{
				"Content-Type":"image/jpeg"
			});
			res.end(data,'binary')
		})
	}
})

function getFile(startPath){
	var results =[];
	function getter(path){
		var files = fs.readdirSync(path);
		files.forEach(function(item,index,arr){
			if(item){
				var fPath = Path.join(path,item);
				var stat = fs.statSync(fPath);
				if(stat.isDirectory()){//是文件夹
					getter(fPath)
				}else if(stat.isFile()){
					results.push(fPath)
				}
			}
		})
	}
	getter(startPath);
	return results;
}

server.listen(3000,'localhost')

